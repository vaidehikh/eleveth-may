export function getApplicationListApi() {

    console.log("getApplicationListApi method call");
    var myHeaders = new Headers();
    myHeaders.append("Access-Control-Allow-Origin",
        "Content-Type", "application/json");
    var requestOptions = {
        method: 'GET',
        redirect: 'follow',
        headers: myHeaders
    };

    return fetch(`http://localhost:3000/applications`, requestOptions)

        .then((res) => {

            return res.json();
        })

        .catch(err => err);
}