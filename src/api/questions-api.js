export function getQuestionListApi() {

    console.log("getQuestionListApi method call");
    var myHeaders = new Headers();
    myHeaders.append("Access-Control-Allow-Origin",
        "Content-Type", "application/json");
    var requestOptions = {
        method: 'GET',
        redirect: 'follow',
        headers: myHeaders
    };

    return fetch(`http://localhost:3000/questions`, requestOptions)

        .then((res) => {

            return res.json();
        })

        .catch(err => err);
}