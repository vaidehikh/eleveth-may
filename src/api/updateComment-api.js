export function updateCommentApi(comments, id){
    console.log("updateCommentApi method call "  + comments +id );
   
    var requestOptions = {
        method: 'PUT',
         redirect: 'follow',
         headers: {
            "Content-Type": "application/json",
           
        },
        body: JSON.stringify({comments, id})
    };

    return fetch(`http://localhost:3000/applications${id}`, requestOptions)

        .then((res) => {
            if (res.status === 401) {
                return 'invalid authentication';
            } else if (res.status !== 200) {
                return 'failed';
            }
           
            return res.json();
        })
       
        .catch(err => err);
}

