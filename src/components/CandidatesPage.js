import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { getCandidateList } from "../actions/candidate-actions";
import { connect } from "react-redux";
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';

class CandidatesPage extends Component {
    constructor(props) {
        super(props)
        this.listItemClick = this.listItemClick.bind(this)
    }
    componentDidMount(props) {
        this.props.dispatch(getCandidateList())
    }
    listItemClick(applicationId) {
        console.log(applicationId)
        this.props.history.push(`/candidatedetails/${applicationId}`)
    }

    render() {
        return (
            console.log(this.props.candidateList),
            <div>

                <div className="container">

                    <h1>candidates</h1>
                    {this.props.candidateList.map((item, i) => (
                        console.log(item.name),
                        <List component="nav" key={item.id}>
                            <ListItem onClick={() => this.listItemClick(item.applicationId)}>
                                {item.name}
                            </ListItem>

                        </List>
                    ))}
                </div>

            </div>
        )
    }
}

const select = (state) => ({
    candidateList: state.candidates.candidateList
});
export default withRouter(connect(select)(CandidatesPage))