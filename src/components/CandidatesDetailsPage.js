import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { getApplicationList, getQuestionList, updateComment } from "../actions/Actions";
import Button from '@material-ui/core/Button';
import { Input } from 'reactstrap';
import { Player, ControlBar } from 'video-react';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';

class CandidatesDetailsPage extends Component {

    constructor(props) {
        super(props)
        this.state = {
            comment: '',
            id: '',

        }
        this.onChange = this.onChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.updateResponse = this.updateResponse.bind(this)
    }

    onChange(event) {
        this.setState({
            comment: event.target.value,
            id: event.target.id
        });

    }
    handleSubmit() {
        const { comment, id } = this.state;
        this.props.dispatch(updateComment(comment, id))
        return this.updateResponse()
    }
    updateResponse() {
        if (this.props.updateStatus !== 'sucess') {
            alert("Updating Comments Failed")
        }
        else {
            alert("Updating Comments Sucess")
        }
    }

    componentDidMount(props) {
        this.props.dispatch(getApplicationList())
        this.props.dispatch(getQuestionList())
    }
    render() {

        const root = {
            marginLeft: '240px',
            marginRight: '240px',
        };
        const media = {
            maxWidth: "800px",
            height: "40%",
            paddingTop: '20px',
            display: 'flex',
            alignItems: "center",
            justifyContent: "center"
        };
        return (

            this.props.match.params.applicationId !== 'undefined' ?
                <div>
                    {this.props.applicationList.map((item, i) => (
                        item.id == this.props.match.params.applicationId ?
                            <div>
                                <h1>CandidatesDetailsPage</h1>

                                {item.videos.map((data, j) => (
                                    this.props.questionList.map((value, k) => (

                                        value.id === data.questionId ?
                                            <div >

                                                <Card style={root}>
                                                    <CardHeader title={value.question} />
                                                    <CardMedia style={media}>
                                                        <Player fluid={false}
                                                            ref={player => { this.player = player; }}>
                                                            <source src={data.src} />
                                                            <ControlBar autoHide={false} />
                                                        </Player>

                                                    </CardMedia>

                                                    <CardActions>
                                                        <Input type="text" id={item.id} name="comment"
                                                            onChange={this.onChange} />

                                                        <Button color="primary"

                                                            onClick={this.handleSubmit}>Submit</Button>
                                                    </CardActions>
                                                </Card>
                                            </div>

                                            : <div></div>

                                    ))
                                ))}

                            </div>
                            : <div></div>

                    ))}

                </div>

                : <div>
                    <h2>Sorry the candidate did not have any applications</h2>
                </div>

        )
    }
}

const select = (state) => ({
    applicationList: state.applications.applicationList,
    questionList: state.questions.questionList,
    updatedComments: state.comments.updatedComments,
    updateStatus: state.status.updateStatus
});
export default withRouter(connect(select)(CandidatesDetailsPage))

