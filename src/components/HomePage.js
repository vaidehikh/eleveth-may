import React, { Component } from 'react';
import { withRouter, Route, Switch } from 'react-router-dom';
import CandidatesPage from './CandidatesPage';
import CandidatesDetailsPage from './CandidatesDetailsPage';

export class HomePage extends Component {
    constructor(props) {
        super(props);
        this.candidateList = this.candidateList.bind(this)
    }
    candidateList() {
        this.props.history.push(`/candidates`)
    }

    render() {
        return (
            <div>
                <h2 onClick={this.candidateList}>Candidates For Job</h2>

                <Switch>

                    <Route exact path='/candidates' component={CandidatesPage} />
                    <Route exact path='/candidatedetails/:applicationId' component={CandidatesDetailsPage} />
                </Switch>

            </div>
        )
    }
}
export default withRouter(HomePage);