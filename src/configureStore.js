import { combineReducers, createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import candidates from './reducers/candidates-reducers'
import applications from './reducers/applications-reducers'
import questions from './reducers/questions-reducers'
import comments from './reducers/updateComments-reducers'
import status from './reducers/updateComments-reducers'

const reducers = combineReducers({
  candidates,
  applications,
  questions,
  comments,
  status
  });
  const initialState = {};
  const enhancers = [];
  const middleware = [thunk];

  const { devToolsExtension } = window;
    if (typeof devToolsExtension === 'function') {
      enhancers.push(devToolsExtension());
    } 
  //}
  
  
  const composedEnhancers = compose(applyMiddleware(...middleware), ...enhancers);
  
  const store = createStore(reducers, initialState, composedEnhancers);


  
  export default store;