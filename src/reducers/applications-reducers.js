import {
  GET_APPLICATION_LIST_INIT,
  GET_APPLICATION_LIST_SUCCESS,
  GET_APPLICATION_LIST_FAILED
} from '../actions/Actions';

export const initialState = {

  applicationList: []
};

export default function candidateReducer(state = initialState, action) {
  const handlers = {
    [GET_APPLICATION_LIST_INIT]: (state) => ({
      ...state,
    }),
    [GET_APPLICATION_LIST_SUCCESS]: (state, action) => ({
      ...state,
      applicationList: action.response,

    }),
    [GET_APPLICATION_LIST_FAILED]: (state) => ({
      ...state,
      applicationList: [],
    }),

  };

  const handler = handlers[action.type];
  if (!handler) return state;
  return { ...state, ...handler(state, action) };
}

