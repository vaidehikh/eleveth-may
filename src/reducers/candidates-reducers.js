import {
  GET_CANDIDATE_LIST_INIT,
  GET_CANDIDATE_LIST_SUCCESS,
  GET_CANDIDATE_LIST_FAILED
} from '../actions/Actions';

export const initialState = {

  candidateList: []
};

export default function candidateReducer(state = initialState, action) {
  const handlers = {
    [GET_CANDIDATE_LIST_INIT]: (state) => ({
      ...state,
    }),
    [GET_CANDIDATE_LIST_SUCCESS]: (state, action) => ({
      ...state,
      candidateList: action.response,

    }),
    [GET_CANDIDATE_LIST_FAILED]: (state) => ({
      ...state,
      candidateList: [],
    }),

  };

  const handler = handlers[action.type];
  if (!handler) return state;
  return { ...state, ...handler(state, action) };
}

