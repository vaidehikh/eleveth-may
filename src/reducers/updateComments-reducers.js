import {
  UPDATE_COMMENTS_INIT,
  UPDATE_COMMENTS_SUCCESS,
  UPDATE_COMMENTS_FAILED
} from '../actions/Actions';

export const initialState = {

  updatedComments: [],
  updateStatus: ''
};

export default function updateCommentsReducer(state = initialState, action) {
  const handlers = {
    [UPDATE_COMMENTS_INIT]: (state) => ({
      ...state,
    }),
    [UPDATE_COMMENTS_SUCCESS]: (state, action) => ({
      ...state,
      updatedComments: action.response,
      updateStatus: action.response
    }),
    [UPDATE_COMMENTS_FAILED]: (state) => ({
      ...state,
      updatedComments: [],
      updateStatus: action.response
    }),

  };

  const handler = handlers[action.type];
  if (!handler) return state;
  return { ...state, ...handler(state, action) };
}