import {
  GET_QUESTION_LIST_INIT,
  GET_QUESTION_LIST_SUCCESS,
  GET_QUESTION_LIST_FAILED
} from '../actions/Actions';

export const initialState = {

  questionList: []
};

export default function candidateReducer(state = initialState, action) {
  const handlers = {
    [GET_QUESTION_LIST_INIT]: (state) => ({
      ...state,
    }),
    [GET_QUESTION_LIST_SUCCESS]: (state, action) => ({
      ...state,
      questionList: action.response,

    }),
    [GET_QUESTION_LIST_FAILED]: (state) => ({
      ...state,
      questionList: [],
    }),

  };

  const handler = handlers[action.type];
  if (!handler) return state;
  return { ...state, ...handler(state, action) };
}

