import { 
    getCandidateListApi
  } from '../api/candidate-api';

export const GET_CANDIDATE_LIST_INIT = 'GET_CANDIDATE_LIST_INIT';
export const GET_CANDIDATE_LIST_SUCCESS = 'GET_CANDIDATE_LIST_SUCCESS';
export const GET_CANDIDATE_LIST_FAILED = 'GET_CANDIDATE_LIST_FAILED';

export function getCandidateList() {
    console.log('getCandidateList')
    return function (dispatch) {
      
      dispatch({ type: GET_CANDIDATE_LIST_INIT });
      console.log('dispatch')
      getCandidateListApi()
      
      .then((response) => {
      
            return dispatch({ type: GET_CANDIDATE_LIST_SUCCESS, response })
         
        })
        
        .catch(error => dispatch({ type: GET_CANDIDATE_LIST_FAILED, error }));
    };
}

