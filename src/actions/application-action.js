
import {
    getApplicationListApi
} from '../api/applications-api';

export const GET_APPLICATION_LIST_INIT = 'GET_APPLICATION_LIST_INIT';
export const GET_APPLICATION_LIST_SUCCESS = 'GET_APPLICATION_LIST_SUCCESS';
export const GET_APPLICATION_LIST_FAILED = 'GET_APPLICATION_LIST_FAILED';

export function getApplicationList() {
    console.log('getCandidateList')
    return function (dispatch) {

        dispatch({ type: GET_APPLICATION_LIST_INIT });
        console.log('dispatch')
        getApplicationListApi()

            .then((response) => {

                return dispatch({ type: GET_APPLICATION_LIST_SUCCESS, response })

            })

            .catch(error => dispatch({ type: GET_APPLICATION_LIST_FAILED, error }));
    };
}

