import {
    getQuestionListApi
} from '../api/questions-api';

export const GET_QUESTION_LIST_INIT = 'GET_QUESTION_LIST_INIT';
export const GET_QUESTION_LIST_SUCCESS = 'GET_QUESTION_LIST_SUCCESS';
export const GET_QUESTION_LIST_FAILED = 'GET_QUESTION_LIST_FAILED';

export function getQuestionList() {
    console.log('getQuestionList')
    return function (dispatch) {

        dispatch({ type: GET_QUESTION_LIST_INIT });
        console.log('dispatch')
        getQuestionListApi()

            .then((response) => {

                return dispatch({ type: GET_QUESTION_LIST_SUCCESS, response })

            })

            .catch(error => dispatch({ type: GET_QUESTION_LIST_FAILED, error }));
    };
}

