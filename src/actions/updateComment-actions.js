import {
    updateCommentApi
} from '../api/updateComment-api';

export const UPDATE_COMMENTS_INIT = 'UPDATE_COMMENTS_INIT';
export const UPDATE_COMMENTS_SUCCESS = 'UPDATE_COMMENTS_SUCCESS';
export const UPDATE_COMMENTS_FAILED = 'UPDATE_COMMENTS_FAILED';

export function updateComment(comments, id){
    return function (dispatch){
        dispatch({ type: UPDATE_COMMENTS_INIT });
        updateCommentApi(comments, id)
        .then((response) => {
            return dispatch({ type: UPDATE_COMMENTS_SUCCESS, response })
        }) 
        .catch(error => dispatch({ type: UPDATE_COMMENTS_FAILED, error }))
    }
}
