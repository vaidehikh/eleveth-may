import React from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Route } from 'react-router-dom';

import { createBrowserHistory } from "history";

import CandidatesPage from './components/CandidatesPage';
import { HomePage } from './components/HomePage';

function App() {
  return (
    <div className="App">
        <BrowserRouter history={createBrowserHistory}>
        
        <Route exact path='' component={HomePage} />
      
    </BrowserRouter>
      {/* <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header> */}
    </div>
  );
}

export default App;
